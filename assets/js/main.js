$(document).ready(function(){
  headerInit({
    parent:'#global-header .container',
    wrapper:true,
    responsive_only_items:[
      '#top-nav-toggle',
    ]
  });
});
function headerInit(params) {
  if(params.parent && params.parent.length > 0) { //Main params check
    //Wrapper setup
    let element_to_modify;
    if(params.wrapper){
      element_to_modify = $(params.parent).parent();
    }
    else {
      element_to_modify = $(params.parent);
    }
    //Set responsive items
    params.responsive_only_items.forEach(function(item, index){
      let dom_item = document.querySelectorAll(item);
      params.responsive_only_items[index] = dom_item[0];
    });
    //Resize event
    $(window).on('resize',()=>{
      let largeChildren = childrenLargerThanParent(params.parent,params.responsive_only_items);
      if(largeChildren) {
        element_to_modify.addClass('responsive');
      }
      else {
        element_to_modify.removeClass('responsive');
      }
    }).trigger('resize');
  }
  else {
    //Faulty selector exit function
    console.warn('Function "HeaderInit" wasn\'t provided with valid selectors. Exitting function');
    return;
  }
}
function childrenLargerThanParent(parent_selector,skip_items) {
  let parent = $(parent_selector);
  let children = parent.children();
  let children_width = 0;
  children.each(function(){
    if(skip_items.indexOf($(this).get(0)) !== 0){
      children_width += $(this).outerWidth();
    }
  });
  if(children_width > parent.width()){
    return true;
  }
  else {
    return false;
  }
}
